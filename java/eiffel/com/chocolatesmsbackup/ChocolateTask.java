/**
 * Chocolate SMS Backup - SMS Backup application for Android KitKat and more
 *
 * Copyright 2017 Francis Laniel
 *
 * This file is part of Chocolate SMS Backup.
 *
 * Chocolate SMS Backup is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chocolate SMS Backup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chocolate SMS Backup.  If not, see <http://www.gnu.org/licenses/>.
 */

package eiffel.com.chocolatesmsbackup;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;

/**
 * Class which handles information about progress of {@link ExporterTask} and
 * {@link ImporterTask}.
 */
public abstract class ChocolateTask extends AsyncTask<ChocolateFile, Integer, Long >{
	protected Exception error;

	protected BackupActivity activity;
	protected ProgressDialog progress;
	protected AlertDialog resultDialog;
	protected boolean cancel;
	protected boolean badArgc;

	public ChocolateTask(BackupActivity activity, ProgressDialog progress, AlertDialog
					resultDialog) {
		this.activity = activity;
		this.progress = progress;
		this.resultDialog = resultDialog;

		this.cancel = false;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);

		if (values.length >= 1)
			progress.setProgress(values[0]);
	}

	/**
	 * Print an alert dialog with some information
	 *
	 * @param result if null the alter dialog will print information about the
	 * error else it will print how many SMS has been exported
	 */
	@Override
	protected void onPostExecute(Long result) {
		progress.dismiss();

		if (result == null) {
			if (badArgc)
				resultDialog.setMessage(activity.getString(R.string.error_argc) + activity
								.getString(R.string.error_contact));

			if (error != null)
				resultDialog.setMessage(activity.getString(R.string.error_exception) +
								error.getMessage() + activity.getString(R.string.error_contact));
		} else {
			resultDialog.setMessage("'" + result + "'" + activity.getString(R.string
							.exported));
		}

		resultDialog.show();
		activity.recreate();
	}

	/**
	 * Setter used to cancel the execution of the {@link ExporterTask}.
	 * If true the task is canceled.
	 *
	 * @param cancel
	 */
	public void setCancel(boolean cancel) {
		this.cancel = cancel;
	}
}
