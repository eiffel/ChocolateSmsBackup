# Chocolate SMS Backup

A little Android application to export and import your SMS.

## Export

SMS are saved in an XML file named like `HH_MM_SS_YYYY-MM-DD_sms.xml`.

All files are saved in `/sdcard/Chocolate SMS Backup/`.

## Import

...

## License

This software is **libre** software licensed under the **GNU General Public License
3**.

## Bugs and issues

Actually the application is under test and is not finished.

By the way if you find any bug please open a new issue and provide as much
details as you can.