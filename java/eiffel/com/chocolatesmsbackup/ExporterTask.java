/**
 * Chocolate SMS Backup - SMS Backup application for Android KitKat and more
 *
 * Copyright 2017 Francis Laniel
 *
 * This file is part of Chocolate SMS Backup.
 *
 * Chocolate SMS Backup is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chocolate SMS Backup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chocolate SMS Backup.  If not, see <http://www.gnu.org/licenses/>.
 */

package eiffel.com.chocolatesmsbackup;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class which contains methods inherited from AsyncTask for export data from
 * ContentProvider to XML file.
 *
 * @see android.os.AsyncTask
 */
public class ExporterTask extends ChocolateTask {
	private static final String TAG = "ExporterTask";


	public ExporterTask(BackupActivity activity, ProgressDialog progress, AlertDialog
					resultDialog) {
		super(activity, progress, resultDialog);
	}

	/**
	 * Create filename.xml and put data from the contentProvider targeted by
	 * String content
	 *
	 * @param chocoFile
	 */
	@Override
	protected Long doInBackground(ChocolateFile... chocoFile) {
		int i;

		String type;
		String filename;
		String content;

		Integer count;
		Integer total;

		Uri uri;

		ChocolateCursor cursor;

		FileWriter file;
		XmlSerializer serializer;

		Pattern pattern;
		Matcher match;

		if (chocoFile.length == 0) {
			badArgc = true;

			return null;
		}

		type = chocoFile[0].getType();
		filename = chocoFile[0].getFileName();
		content = chocoFile[0].getUri();

		uri = Uri.parse(content);

		// ChocolateCursor is a simple Cursor with a method (getValue) in more
		cursor = new ChocolateCursor(activity.getContentResolver().query(uri, null,
						null, null, null));

		serializer = Xml.newSerializer();

		count = 0;
		total = cursor.getCount();

		pattern = Pattern.compile(BaseColumns._ID);

		try {
			file = new FileWriter(filename);

			serializer.setOutput(file);

			serializer.startDocument("UTF-8", true);

			serializer.startTag("", type);
			serializer.attribute("", "number", String.valueOf(total));

			progress.setMax(total);

			Log.d(TAG, "Have to export " + total + " object(s)");

			while(cursor.moveToNext() && !cancel){
				serializer.startTag("", "exported");


				for (i = 0; i < cursor.getColumnCount(); i++) {
					/*
					 * Add field only if its name does not match BaseColumns._ID ("_id").
					 * The XML file will not contain any id so when importing data the
					 * new id will be given by sqlite so it will not be any id exception.
					 */
					match = pattern.matcher(cursor.getColumnName(i));

					if (!match.matches())
						serializer.attribute("", cursor.getColumnName(i), cursor.getValue
										(i));
				}


				serializer.endTag("", "exported");

				count++;
				publishProgress(count);
			}

			serializer.endTag("", type);

			serializer.endDocument();
		} catch (IOException e) {
			error = e;

			return null;
		}

		return Long.valueOf(count);
	}
}
