/**
 * Chocolate SMS Backup - SMS Backup application for Android KitKat and more
 *
 * Copyright 2017 Francis Laniel
 *
 * This file is part of Chocolate SMS Backup.
 *
 * Chocolate SMS Backup is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chocolate SMS Backup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chocolate SMS Backup.  If not, see <http://www.gnu.org/licenses/>.
 */

package eiffel.com.chocolatesmsbackup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

/**
 * Class which extends ProgressDialog and permit to stop the ExporterTask
 */
public class ChocolateProgressDialog extends ProgressDialog {
	private ChocolateTask chocoTask;

	/**
	 * Constructor for ChocolateProgressDialog
	 * @param context
	 * @param title the title of the {@link ProgressDialog}
	 * @param button the String of the button of the {@link ProgressDialog}
	 */
	public ChocolateProgressDialog(Context context, String
	                               title, String button) {
		super(context);

		setTitle(title);
		setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		setButton(ProgressDialog.BUTTON_NEGATIVE, button,	new DialogInterface
						.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				/*
				 * The ExporterTask can be not assigned so we need to check if it is
				 * null to avoid NullPointerException
				 */
				if (chocoTask != null)
					chocoTask.setCancel(true);
			}
		});
	}

	/**
	 * Constructor for ChocolateProgressDialog
	 * @param context
	 * @param title the title of the {@link ProgressDialog}
	 * @param filename the message of the {@link ProgressDialog}
	 * @param negative the String of the negative button of the
	 * {@link ProgressDialog}
	 * @param positive the String of the positive button of {@link ProgressDialog}
	 */
	public ChocolateProgressDialog(Context context, String title, String
					filename, String negative, String positive) {
		super(context);

		setTitle(title);
		setMessage(filename);
		setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		setButton(ProgressDialog.BUTTON_NEGATIVE, negative,	new DialogInterface
						.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				/*
				 * The Task can be not assigned so we need to check if it is
				 * null to avoid NullPointerException
				 */
				if (chocoTask != null)
					chocoTask.setCancel(true);
			}
		});

		setButton(ProgressDialog.BUTTON_POSITIVE, positive,	new DialogInterface
						.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO start importerTask
			}
		});
	}

	/**
	 * Setter used to assign an {@link ExporterTask} to this
	 * {@link ChocolateProgressDialog}.
	 *
	 * It can not be done in the constructor because
	 * {@link ChocolateProgressDialog} is needed by constructor of
	 * {@link ExporterTask}.
	 *
	 * @param chocoTask the {@link ChocolateTask} to assign
	 */
	public void setChocolateTask(ChocolateTask chocoTask) {
		this.chocoTask = chocoTask;
	}
}
