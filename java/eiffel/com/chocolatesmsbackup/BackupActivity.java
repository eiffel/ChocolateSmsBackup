/**
 * Chocolate SMS Backup - SMS Backup application for Android KitKat and more
 *
 * Copyright 2017 Francis Laniel
 *
 * This file is part of Chocolate SMS Backup.
 *
 * Chocolate SMS Backup is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chocolate SMS Backup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chocolate SMS Backup.  If not, see <http://www.gnu.org/licenses/>.
 */

package eiffel.com.chocolatesmsbackup;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Main activity of the Chocolate SMS Backup application.
 * It permits to export and import SMS
 */
public class BackupActivity extends AppCompatActivity {
	private static final String TAG = "BackupActivity";

	private static final String FORMAT = "%s/%tH_%tM_%tS_%tF";

	private static final String SMS_TYPE = "_sms";
	private static final String XML = ".xml";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		String workingDirectory;

		File chocoDir;

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_backup);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		workingDirectory = Environment.getExternalStorageDirectory() +
						"/" + getString(R.string.app_name) + "/";

		ChocolateApplication.setWorkingDirectory(workingDirectory);

		chocoDir = new File(workingDirectory);

		if (!chocoDir.exists())
			if (!chocoDir.mkdir()) // try to create /sdcard/app_name directory
				Log.e(TAG, "Error trying to create directory '" + workingDirectory +
								"'");
			else
				Log.i(TAG, "Create directory : '" + workingDirectory + "'");
		else
			Log.i(TAG, "'" + workingDirectory + "' already exists");
	}

	/**
	 * This method will print all the XML files present in /sdcard/app_name/
	 */
	@Override
	protected void onStart() {
		int i;

		File chocoDir;
		File[] xmlFiles;

		ListView listView;
		ChocolateAdapter chocoAdapter;

		ChocolateFile chocoFile;
		ArrayList<ChocolateFile> chocoFiles;

		super.onStart();

		chocoDir = new File(ChocolateApplication.getWorkingDirectory());
		xmlFiles = chocoDir.listFiles();

		chocoFiles = new ArrayList<ChocolateFile>();

		for (i = 0; i < xmlFiles.length; i++) {
			try {
				chocoFile = new ChocolateFile(this, xmlFiles[i].getName());

				chocoFiles.add(chocoFile);
			} catch (NoSuchFieldException e) {
				/*
				 * This exception is not very serious.
				 * It just says that the file is not an xml file or that its name
				 * does not match "_sms.xml".
				 * So I just print information about this exception in the Log
				 * without any other processing.
				 * The file will not be added to the list and it will not be displayed.
				 */
				e.printStackTrace();
			}
		}

		chocoAdapter = new ChocolateAdapter(this, chocoFiles);

		listView = (ListView) findViewById(R.id.list_view);

		listView.setAdapter(chocoAdapter);

		registerForContextMenu(listView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_backup, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id;

		ExporterTask exporterTask;
		ChocolateProgressDialog progress;

		Calendar c;
		String filename;

		ChocolateFile chocoFile;

		AlertDialog dialog;

		String title;

		id = item.getItemId();

		c = Calendar.getInstance();

		if (id == R.id.export) { // export SMS
			filename = String.format(FORMAT + SMS_TYPE + XML,
							ChocolateApplication.getWorkingDirectory(), c, c, c, c);

			title = getString(R.string.action_export);

			dialog = chocolateDialog(title);

			try {
				chocoFile = new ChocolateFile(this, filename);
			} catch (NoSuchFieldException e) {
				dialog.setMessage(getString(R.string.error_exception) + e.getMessage()
								+ getString(R.string.error_contact));

				dialog.show();

				return false;
			}

			/*
			 * Firstly we create a ChocolateProgressDialog.
			 * Then we create an ExporterTask with the ChocolateProgressDialog created
			 * before.
			 * And we assign the ExporterTask created to the ChocolateProgressDialog,
			 * so by clicking on the Dialog's button it cancel the ExporterTask
			 *
			 * I agree that this is not well coded but I did this to avoid cyclic
			 * dependency.
			 * Normally there should be no problem because the user can not push the
			 * Dialog's button before the ExporterTask was created.
			 */

			progress = new ChocolateProgressDialog(this, title, getString(R.string
							.button_cancel));

			exporterTask = new ExporterTask(this, progress, dialog);

			progress.setChocolateTask(exporterTask);

			progress.show();

			exporterTask.execute(chocoFile);

			return true;
		}

		if (id == R.id.about) { // print some information
			dialog = chocolateDialog(getString(R.string.action_about), getString(R
							.string.readme));

			dialog.show();

			return true;
		}

		if (id == R.id.license) { // print license information
			dialog = chocolateDialog(getString(R.string.action_license),
							getString(R.string.license));

			dialog.show();

			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Creates an AlertDialog with a title, a content and a button to hide it.
	 *
	 * @param title title of the AlertDialog
	 * @param content content of the AlertDialog
	 * @return the AlertDialog just created
	 */
	public AlertDialog chocolateDialog(String title, String content) {
		AlertDialog.Builder builder;

		builder = new AlertDialog.Builder(this);

		builder.setTitle(title);
		builder.setMessage(content);

		builder.setNeutralButton(R.string.button_close, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss(); // onClick hide the dialog
			}
		});

		return builder.create();
	}

	/**
	 * Creates an AlertDialog with a title and a button to hide it.
	 * The message will be set later.
	 *
	 * @param title title of the AlertDialog
	 * @return the AlertDialog just created
	 */
	public AlertDialog chocolateDialog(String title) {
		return chocolateDialog(title, "");
	}
}
