/**
 * Chocolate SMS Backup - SMS Backup application for Android KitKat and more
 *
 * Copyright 2017 Francis Laniel
 *
 * This file is part of Chocolate SMS Backup.
 *
 * Chocolate SMS Backup is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chocolate SMS Backup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chocolate SMS Backup.  If not, see <http://www.gnu.org/licenses/>.
 */

package eiffel.com.chocolatesmsbackup;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A model to print easily the information of a file.
 */
public class ChocolateFile {
	// group used for the capture regex
	private static int HOUR = 1; // group 1 is the hour
	private static int MINUTE = 2; // group 2 is the minute
	private static int SECOND = 3; // group 3 is the second
	private static int DATE = 4; // group 4 is the date
	private static int TYPE = 5; // group 5 is the type

	private static final String SMS_REGEX = "(\\d+)_(\\d+)_(\\d+)" +
					"_(\\d+-\\d+-\\d+)_(sms)\\.xml"; /* sms xml files must match this
					regex */

	private static final String SMS_KEY = "sms"; /* type as find in the xml
	name */
	private static final Integer SMS_VALUE = R.string.sms; /* corresponding R
	string */
	private static final String SMS_URI = "content://sms"; /* URI to read and
	write sms */

	private static final String[] REGEX = new String[] { SMS_REGEX };

	/*
   * Those two arrays emulate a Map.
   * They have to be same length and correspondence is done according to the
   * index.
	 */
	private static final String[] KEYS = new String[] { SMS_KEY };
	private static final Integer[] VALUES = new Integer[] { SMS_VALUE };
	private static final String[] URIS = new String[] { SMS_URI };

	private Context context;

	private String fileName;
	private String type;
	private String time;
	private String date;
	private String uri;

	/**
	 * Create a ChocolateFile from the fileName passed as an argument.
	 *
	 * @param context
	 * @param fileName XML filename
	 * @throws NoSuchFieldException instead of returning an empty ChocolateFile
	 * it throws this Exception
	 */
	public ChocolateFile(Context context, String fileName) throws
					NoSuchFieldException {
		int i;

		Pattern pattern;
		Matcher match;

		this.context = context;
		this.fileName = fileName;

		for (i = 0; i < REGEX.length; i++) {
			pattern = Pattern.compile(REGEX[i]);
			match = pattern.matcher(fileName);

			// I use find because fileName can just be a file's name or a path
			if (match.find()) {
				this.time = match.group(HOUR) + ":" + match.group(MINUTE) + ":" +
								match.group(SECOND);
				this.date = match.group(DATE);
				typeAndUriFactory(match.group(TYPE));

				return;
			}
		}
	}

	/**
	 * Each ChocolateFile has a type (for now only SMS are handled).
	 * Type are included in the filename just before the extension.
	 *
	 * This function find the according R string for the type passed as an
	 * argument.
	 * It also set the corresponding URI according to the type
	 *
	 * @param type the function will try to find an according R string and URI
	 *              for this type
	 * @throws NoSuchFieldException if no correspondence between type and R
	 * string was find, the function throw this Exception.
	 */
	private void typeAndUriFactory(String type) throws NoSuchFieldException {
		int i;

		Pattern pattern;
		Matcher match;

		for (i = 0; i < KEYS.length; i++) {
			pattern = Pattern.compile(KEYS[i]);
			match = pattern.matcher(type);

			if (match.matches()) {
				this.type = context.getString(VALUES[i]);

				this.uri = URIS[i];

				return;
			}
		}

		throw new NoSuchFieldException();
	}

	/**
	 * Getter for filename
	 *
	 * @return filename of ChocolateFile
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Getter for date (YYYY-MM-DD)
	 *
	 * @return date of ChocolateFile
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Getter for time (HH:MM:SS)
	 *
	 * @return time of ChocolateFile
	 */
	public String getTime() {
		return time;
	}

	/**
	 * Getter for type.
	 * All type are contained as Integer in VALUES array.
	 * For more details see typeAndUriFactory.
	 *
	 * @return type of ChocolateFile
	 */
	public String getType() {
		return type;
	}

	/**
	 * Getter for content URI.
	 * All URIs are contained as String in URIS array.
	 * For more details see typeAndUriFactory.
	 * @return
	 */
	public String getUri() {
		return uri;
	}
}
