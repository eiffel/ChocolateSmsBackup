/**
 * Chocolate SMS Backup - SMS Backup application for Android KitKat and more
 *
 * Copyright 2017 Francis Laniel
 *
 * This file is part of Chocolate SMS Backup.
 *
 * Chocolate SMS Backup is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chocolate SMS Backup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chocolate SMS Backup.  If not, see <http://www.gnu.org/licenses/>.
 */

package eiffel.com.chocolatesmsbackup;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

/**
 * Class which extends CursorWrapper to add a new method to facilitate
 * writing in XML files.
 *
 * @see android.database.CursorWrapper
 * @see Cursor
 */
public class ChocolateCursor extends CursorWrapper {
	private static final String TAG = "ChocolateCursor";

	/**
	 * Creates a cursor wrapper.
	 *
	 * @param cursor The underlying cursor to wrap.
	 */
	public ChocolateCursor(Cursor cursor) {
		super(cursor);
	}

	/**
	 * Convert the field n°i as a String.
	 * This method was added to simplify the writing inside XML file.
	 *
	 * @param i the number of the field to convert
	 * @return the value of the field n°i converted as a String or an empty
	 * String
	 */
	String getValue(int i) {
		switch(getType(i)){
			case FIELD_TYPE_FLOAT :
				return String.valueOf(getFloat(i));
			case FIELD_TYPE_INTEGER :
				return String.valueOf(getInt(i));
			case FIELD_TYPE_STRING :
				return getString(i);
			default : // handles FIELD_TYPE_NULL and FIELD_TYPE_BLOB
				return "";
		}
	}
}
