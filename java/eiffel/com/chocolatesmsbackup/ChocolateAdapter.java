/**
 * Chocolate SMS Backup - SMS Backup application for Android KitKat and more
 *
 * Copyright 2017 Francis Laniel
 *
 * This file is part of Chocolate SMS Backup.
 *
 * Chocolate SMS Backup is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chocolate SMS Backup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chocolate SMS Backup.  If not, see <http://www.gnu.org/licenses/>.
 */

package eiffel.com.chocolatesmsbackup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * This class is used for printing information about files
 */
public class ChocolateAdapter extends ArrayAdapter<ChocolateFile> {
	private final Context context;

	public ChocolateAdapter(Context context, ArrayList chocoFiles) {
		super(context, 0, chocoFiles);

		this.context = context;
	}

	/**
	 * This method is used to put data of ChocolateFile into the TextView of R
	 * .layout.list_item.
	 * I use this guide :
	 * https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView
	 * to design the two classes ChocolateFile and ChocolateAdapter.
	 *
	 * @param position position of the item in the ListView
	 * @param convertView the View which was created from the ArrayList
	 * @param parent parent View of convertView
	 * @return convertView
	 */
	@NonNull
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		final ChocolateFile chocoFile;

		TextView type;
		TextView date;
		TextView file;

		chocoFile = getItem(position);

		if (convertView == null)
			convertView = LayoutInflater.from(getContext()).inflate(R.layout
							.list_item, parent, false);

		type = (TextView) convertView.findViewById(R.id.type);
		date = (TextView) convertView.findViewById(R.id.date);
		file = (TextView) convertView.findViewById(R.id.file);

		/*
		 * AndroidStudio tells me that using getType can throw NullPointerException.
		 * I do no think so, be cause if the the file has no type it is normally not
		 * in the ListView
		 */
		type.setText(chocoFile.getType());
		date.setText(chocoFile.getTime() + "\t" + chocoFile.getDate());
		file.setText(chocoFile.getFileName());

		convertView.setOnClickListener(new View.OnClickListener() {
			/**
			 * On click this method print a progressDialog from which the user can
			 * begin the import of the file.
			 *
			 * @param v the View corresponding to the clicked item
			 */
			@Override
			public void onClick(View v) {
			chocolateProgressDialogShow(position);
			}
		});

		convertView.setOnLongClickListener(new View.OnLongClickListener() {
			/**
			 * On long click this method print a menu with 2 choices :
			 * import the file,
			 * remove the file
			 *
			 * @param v the view corresponding to the long-clicked item
			 * @return always true
			 */
			@Override
			public boolean onLongClick(View v) {
				final ChocolateFile chocoFile;

				PopupMenu popupMenu;

				MenuInflater inflater;
				Menu menu;

				chocoFile = getItem(position);

				popupMenu = new PopupMenu(v.getContext(), v);

				inflater = popupMenu.getMenuInflater();

				menu = popupMenu.getMenu();

				inflater.inflate(R.menu.menu_file, menu);

				/*
				 * First item of the menu is not clickable and contains the fileName so
				 * the user knows which will be removed or imported
				 */
				menu.findItem(R.id.item_file).setTitle(chocoFile.getFileName());

				popupMenu.show();

				popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						AlertDialog.Builder builder;

						switch(item.getItemId()){
							case R.id.item_import :
								chocolateProgressDialogShow(position);

								return true;
							case R.id.item_remove :
								builder = new AlertDialog.Builder(context);

								builder.setTitle(R.string.item_remove);
								builder.setMessage(chocoFile.getFileName());

								builder.setNegativeButton(R.string.button_cancel, new
												DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss(); // onClick hide the dialog
									}
								});

								builder.setPositiveButton(R.string.item_remove, new
												DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										File file;

										// file is created from the absolute pathname
										file = new File(ChocolateApplication.getWorkingDirectory() +
														chocoFile.getFileName());

										if (file.delete()) {
											remove(chocoFile); // remove item from ListView

											// adapter will refresh view because item has been removed
											notifyDataSetChanged();
										}
									}
								});

								builder.create().show();

								return true;
							default :
								return false;
						}
					}
				});

				return true;
			}
		});

		return convertView;
	}

	/**
	 * This function creates a {@link ChocolateProgressDialog} which will
	 * contain the filename of the {@link ChocolateFile} associated to position.
	 *
	 * @param position the position of the clicked item in ListView.
	 */
	private void chocolateProgressDialogShow(int position){
		String action;

		ChocolateFile chocoFile;
		ChocolateProgressDialog progress;

		chocoFile = getItem(position);

		action = context.getString(R.string.action_import);

		// create application specific progressDialog
		progress = new ChocolateProgressDialog(context, action + " " +
						chocoFile.getType(), chocoFile.getFileName(), context
						.getString(R.string.button_cancel), action);

		progress.show();
	}
}
