/**
 * Chocolate SMS Backup - SMS Backup application for Android KitKat and more
 *
 * Copyright 2017 Francis Laniel
 *
 * This file is part of Chocolate SMS Backup.
 *
 * Chocolate SMS Backup is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chocolate SMS Backup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chocolate SMS Backup.  If not, see <http://www.gnu.org/licenses/>.
 */

package eiffel.com.chocolatesmsbackup;

import android.app.Application;

/**
 * Application class which contains a static value for the application
 * working path.
 */
public class ChocolateApplication extends Application {
	private static String workingDirectory;

	/**
	 * Setter for workingDirectory.
	 * @param s the string to affect to workingDirectory.
	 */
	static void setWorkingDirectory(String s){
		workingDirectory = s;
	}

	/**
	 * Getter for workingDirectory.
	 * @return the current value of workingDirectory.
	 */
	static String getWorkingDirectory(){
		return workingDirectory;
	}
}
